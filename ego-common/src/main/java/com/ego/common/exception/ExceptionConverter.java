package com.ego.common.exception;

import com.ego.common.constant.CommonResponseEnum;
import com.ego.common.constant.Loggers;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionException;

/**
 * 异常转换
 * @author lyy
 */
@Component
public class ExceptionConverter {

//    @Autowired
//    protected DeployEnvironment env;


    /**
     * 异常转换
     *
     * @param e 原始异常
     * @return 转换后异常
     */
    public EgoException toEduException(Throwable e) {
        // === 客户端错误 ===
        if (e instanceof GlobalInvalidArgumentException) {
            return new ClientErrorException(ExceptionUtil.getDetailMessage("客户端异常", e),CommonResponseEnum.ClientError);
        }

        // === 服务端错误 ===
        if (e instanceof EgoException) {
            return (EgoException) e;
        }

        Loggers.Monitor.warn("服务器未知异常，请确认整改", e);

        if (e instanceof IllegalArgumentException) {

            return new ServerErrorException(ExceptionUtil.getDetailMessage("参数错误", e),CommonResponseEnum.IllegalArgument,ExceptionUtil.getDetailMessage("参数错误", e));
        }

        if (e instanceof DataAccessException) {
            return new DbFailureException(ExceptionUtil.getDetailMessage("数据库异常", e),CommonResponseEnum.DbFailure);
        }
        if (e instanceof TransactionException) {
            return new DbFailureException(ExceptionUtil.getDetailMessage("数据库异常", e),CommonResponseEnum.DbTransactionException);
        }
        if (e instanceof NullPointerException) {
            return new ServerErrorException(ExceptionUtil.getDetailMessage("空指针异常", e), CommonResponseEnum.NullPointerException);
        }

        // 说明：服务器未知错误——不允许返回此错误码（因为无助于快速定位接口问题），若发现则需立即整改
        return new ServerErrorException(ExceptionUtil.getDetailMessage("服务器错误", e),CommonResponseEnum.ServerError);
    }

}
