package com.ego.common.exception;

import com.ego.common.constant.ResponseCode;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:42:44
 **/
public class ServerErrorException extends EgoException {
    private String detailMessage;
    public ServerErrorException(String detailMessage, ResponseCode responseCodeEnum, Object... codeMessageArgs) {
        super(responseCodeEnum, codeMessageArgs);
        this.detailMessage = detailMessage;
    }

    public ServerErrorException(String detailMessage, ResponseCode responseCodeEnum) {
        super(responseCodeEnum);
        this.detailMessage = detailMessage;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }
}
