package com.ego.common.exception;

/**
 * @description: 自定义入参非法的异常 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:42:22 <br>
 * @author: lyy <br>
 */
public class GlobalInvalidArgumentException extends RuntimeException {
    private static final long serialVersionUID = -1605515531880249974L;

    /**
     * 无参构造函数
     */
    public GlobalInvalidArgumentException() {
        super();
    }

    /**
     * 标识异常描述信息的构造函数
     *
     * @param message
     */
    public GlobalInvalidArgumentException(String message) {
        super(message);
    }

    /**
     * 包含异常描述信息和异常堆栈的构造函数
     *
     * @param message
     * @param cause
     */
    public GlobalInvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 包含异常堆栈的构造函数
     *
     * @param cause
     */
    public GlobalInvalidArgumentException(Throwable cause) {
        super(cause);
    }
}
