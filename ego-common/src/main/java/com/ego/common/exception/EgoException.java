package com.ego.common.exception;

import com.ego.common.constant.ResponseCode;
import com.ego.common.util.AbstractJsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author lyy
 * @description 异常基类，其他异常类需继承该类
 * @date 2021-4-29 16:40:04
 **/
@Setter
@Getter
public class EgoException extends RuntimeException{
    /**
     * 应答编码
     */
    private ResponseCode responseCodeEnum;

    /**
     * 用于替换应答消息模板中{@link ResponseCode#getMessage()}的变量数组（数组长度需等于{@link ResponseCode#getArgs()}）
     */
    private Object[] args;


    public EgoException(ResponseCode responseCodeEnum, Object... codeMessageArgs){
        super(EgoException.detailMessage(responseCodeEnum,codeMessageArgs));
        this.responseCodeEnum=responseCodeEnum;
        this.args=codeMessageArgs;
    }


    public EgoException(ResponseCode responseCodeEnum){
        super(responseCodeEnum.getMessage());
        this.responseCodeEnum=responseCodeEnum;
    }

    /**
     * @param responseCodeEnum
     * @param args
     * @return
     */
    public static String detailMessage(ResponseCode responseCodeEnum,Object[] args) {
        if (Objects.isNull(responseCodeEnum)) {
            return "[warn] code is null";
        } else {
            if (responseCodeEnum.getArgs() > 0) {
                if (Objects.isNull(args)) {
                    return "[warn] args is null";
                }

                if (responseCodeEnum.getArgs() != args.length) {
                    return String.format("[warn] args length mismatch: expect %d, actual %d", responseCodeEnum.getArgs(), args.length);
                }
            }

            return String.format(responseCodeEnum.getMessage(), args);
        }
    }

    @Override
    public String toString() {
        return AbstractJsonUtil.getIndentNonEmptyJsonString(this);
    }
}
