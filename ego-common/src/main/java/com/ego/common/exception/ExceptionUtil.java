package com.ego.common.exception;


import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author lyy
 */
public abstract class ExceptionUtil {

    private static final String PKG_PREFIX = "cn.com.ocj.";

    /**
     * 获取异常堆栈信息
     * - 只记录以 PKG_PREFIX 开头的类名信息
     *
     * @param ex 异常
     * @return 堆栈信息
     */
    public static String getSimpleStackString(Throwable ex) {
        StringBuilder ret = new StringBuilder(512);
        ret.append(ex.getMessage()).append("\n");
        Arrays.stream(ex.getStackTrace())
                .filter(m -> m.getClassName().startsWith(PKG_PREFIX))
                .forEach(m -> ret.append(m.toString()).append("\n"));
        return ret.toString();
    }

    /**
     * 获取全量异常堆栈信息+传入的
     *
     * @param ex             异常
     * @param exceptionPoint 异常位置
     * @return 堆栈信息
     */
    public static String getFullStackString(Throwable ex, String exceptionPoint) {
        StringBuilder fullStack = getFullStackStringBuilder(ex);
        fullStack.append("异常位置:").append(exceptionPoint).append("\n");
        return fullStack.toString();
    }

    /**
     * 获取全量异常堆栈信息
     *
     * @param ex 异常
     * @return 堆栈信息
     */
    public static String getFullStackString(Throwable ex) {
        StringBuilder ret = getFullStackStringBuilder(ex);
        return ret.toString();
    }

    private static StringBuilder getFullStackStringBuilder(Throwable ex) {
        StringBuilder ret = new StringBuilder(1024);

        if (ex instanceof UndeclaredThrowableException) {
            ret.append("接口超时：");
            hystrixException(ex, ret);
        } else {
            String message = ex.getMessage();
            if (message != null && message.contains("Hystrix circuit short-circuited and is OPEN")) {
                ret.append("接口熔断：");
                hystrixException(ex, ret);
            } else if (message != null && message.contains("JMS")) {
                ret.append("接口断连：");
                jmsException(ex, ret);
            } else {
                ret.append(message).append("\n");
                Arrays.stream(ex.getStackTrace())
                        .forEach(m -> ret.append(m.toString()).append("\n"));

                Throwable cause = ex.getCause();
                while (cause != null) {
                    ret.append("cause: ").append(cause.getMessage()).append("\n");
                    Arrays.stream(cause.getStackTrace())
                            .forEach(m -> ret.append(m.toString()).append("\n"));
                    cause = cause.getCause();
                }
            }
        }

        return ret;
    }

    private static void hystrixException(Throwable ex, StringBuilder ret) {
        StackTraceElement api = Arrays.stream(ex.getStackTrace())
                .filter(m -> m.getClassName().startsWith(PKG_PREFIX))
                .filter(m -> m.getClassName().contains(".adaptor.") && !m.getClassName().contains("BySpringCGLIB"))
                .findFirst().orElse(null);
        if (api != null) {
            ret.append(api.getClassName()).append(".").append(api.getMethodName());
        } else {
            ret.append("未知接口");
        }
    }

    private static void jmsException(Throwable ex, StringBuilder ret) {
        String message = ex.getMessage();
        if (message.contains("Reason:") && message.contains("broker")) {
            ret.append(message.substring(message.lastIndexOf("javax.jms.JMSException: ")));
        } else {
            Throwable cause = ex.getCause();
            while (cause != null) {
                message = ex.getMessage();
                if (message.contains("Reason:") && message.contains("broker")) {
                    ret.append(message.substring(message.lastIndexOf("javax.jms.JMSException: ")));
                    break;
                }
                cause = cause.getCause();
            }
        }
    }

    public static String getDetailErrorMessage(String basicErrorMessage, Throwable cause, Object... params) {
        StringBuilder message = new StringBuilder(512);

        message.append(basicErrorMessage);

        while (Objects.nonNull(cause)) {
            message.append("; cause-").append(getErrorMessage(cause));
            cause = cause.getCause();
        }

        if (Objects.nonNull(params) && params.length > 0) {
            for (Object param : params) {
                if (null != param) {
                    message.append("; param-").append(param.getClass().getSimpleName()).append(":").append(param);
                }
            }
        }

        return message.toString();
    }

    public static String getDetailMessage(String message, Throwable ex) {
        return message + "：" + getErrorMessage(ex) + ", cause: " + getErrorMessage(ex.getCause());
    }

    public static String getDetailMessage(Throwable ex) {
        return getErrorMessage(ex) + ", cause: " + getErrorMessage(ex.getCause());
    }

    public static String getErrorMessage(Throwable e) {
        if (Objects.isNull(e)) {
            return "";
        }
        return e.getClass().getSimpleName() + ": " + e.getMessage();
    }
}
