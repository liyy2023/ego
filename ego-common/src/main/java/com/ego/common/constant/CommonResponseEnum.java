package com.ego.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用应答编码
 * - 各应用（平台或中心）分别定义自有应答编码表
 * <p>
 * - 应答编码规则：
 * -   八位字符串
 * -     {应用：两位}{子领域：两位}{具体编码：四位}
 * -   应用：两位划分
 * -     通用
 * -       99
 * -     平台
 * -       1x-2x
 * -     中心
 * -       3x-8x
 * - 应答消息规则：
 * -   中文字符串
 * -   前端可以直接展示给用户
 * -   不支持国际化
 */

/**
 * @description: 通用应答编码
 *  * - 各应用（平台或中心）分别定义自有应答编码表 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:33:55 <br>
 * @author: lyy <br>
 */
@Getter
@AllArgsConstructor
public enum CommonResponseEnum implements ResponseCode {
    Success("99990000", "操作成功", 0),
    ClientError("99990006","客户端异常",0),
    ServerError("99990001", "服务器错误", 0),
    IllegalArgument("99990002", "参数错误：%s", 1),
    NoPermission("99990003", "无权限", 0),
    NeedRefreshToken("99990004", "需要刷新Token的TTL", 0),
    NotLogin("99990005", "用户未登录", 0),

    NotFound("99990011", "不存在", 0),
    AlreadyExists("99990012", "已存在", 0),

    AlreadyLocked("99990021", "已锁定", 0),
    LockFailure("99990022", "锁定异常", 0),

    DbFailure("99990031", "数据库异常", 0),
    NetworkFailure("99990032", "网络异常", 0),
    AdaptorFailure("99990033", "外部接口异常", 0),
    DbTransactionException("99990034","数据库事务异常",0),

    ServerShutDown("99990098", "服务器关闭", 0),
    NullPointerException("99990100","空指针异常",0),
    ServerBusy("99990099", "服务器忙", 0);

    /**
     * 应答编码
     */
    private String code;

    /**
     * 应答消息模板
     */
    private String message;

    /**
     * 应答消息模板中包含的变量总数
     */
    private int args;
}
