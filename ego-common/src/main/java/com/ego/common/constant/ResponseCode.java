package com.ego.common.constant;

/**
 * @description: 应答编码定义接口 <br>
 * @author lyy
 * @date 2021-4-29 16:36:15
 * @param:
 */
public interface ResponseCode {

    /**
     * @return 应答编码Enum名称
     */
    String name();

    /**
     * @return 应答编码
     */
    String getCode();

    /**
     * @return 应答消息模板
     */
    String getMessage();

    /**
     * @return 应答消息模板中包含的变量总数
     */
    int getArgs();

    /**
     * @return 应答编码详细描述
     */
    default String description() {
        return name() + '-' + getCode() + '-' + getMessage() + '-' + getArgs();
    }
}
