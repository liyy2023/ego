package com.ego.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 接口类型
 * @author lyy
 * @date 2021-4-29 16:35:18
 */
@Getter
@AllArgsConstructor
public enum InterfaceType {
    //================MQ接口====================
    MQ_RECEIVE("接收MQ消息"),
    MQ_SEND("发送MQ消息"),
    MQ_RESEND("重发MQ消息"),

    //================定时器接口====================
    TIMER_TRIGGER("定时触发"),

    //================Dubbo接口====================
    DUBBO_CALL("调用Dubbo接口"),

    //================REST接口====================
    REST_CALL("调用REST接口"),

    //================SOAP接口====================
    SOAP_CALL("调用SOAP接口"),

    //================FTP接口====================
    FTP_CALL("调用FTP接口"),

    //================其他接口====================
    OTHER_CALL("调用其他接口"),

    //
    ;

    private String desc;

}
