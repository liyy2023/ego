package com.ego.common.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志记录器常量定义
 * @author lyy
 * @date 2021-4-29 16:35:42
 */
public interface Loggers {

    /**
     * 统一监控日志
     */
    Logger Monitor = LoggerFactory.getLogger("monitor");
}
