package com.ego.common.constant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @description: 失败信息 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:34:35 <br>
 * @author: lyy <br>
 *
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FailInfo {
	private String code;
	private String message;
}
