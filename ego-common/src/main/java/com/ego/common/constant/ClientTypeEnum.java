package com.ego.common.constant;


/**
 * @description: 客户端类型 <br>
 * @author lyy
 * @date 2021-4-29 16:33:08
 * @param:
 */
public enum ClientTypeEnum {
    PC_BROWSER, PRO_BROWSER, MOBILE_APP
}
