package com.ego.common.constant;

import com.ego.common.base.Input;
import com.ego.common.util.AbstractParamUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @description: 客户端请求信息类
 * @author lyy
 * @date 2021-4-29 16:32:32
 * @param:
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "客户端")
final public class ClientInfo implements Input {
    private static final long serialVersionUID = -677923676L;

    /**
     * {@link ClientTypeEnum#name()}
     */
    @ApiModelProperty(value = "客户端类型", required = true)
    private String clientType;

    @ApiModelProperty(value = "客户端版本号")
    private String clientVersion;

    @ApiModelProperty(value = "客户端IP", required = true)
    private String clientIp;


    @Override
    public void checkInput() {

        AbstractParamUtil.notBlank(clientType, "客户端类型不能为空");
        AbstractParamUtil.notBlank(clientIp, "客户端IP不能为空");
    }
}
