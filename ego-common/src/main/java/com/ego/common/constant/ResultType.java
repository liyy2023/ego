package com.ego.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 结果类型
 * @author lyy
 * @date 2021-4-29 16:37:33
 */
@Getter
@AllArgsConstructor
public enum ResultType {
    OK("成功"),
    FAIL("失败");

    private String desc;

}
