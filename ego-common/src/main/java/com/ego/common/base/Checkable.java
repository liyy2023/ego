package com.ego.common.base;


/**
 * @author lyy
 * @description 入参校验接口，- 所有入参必须实现此接口
 * @date 2021-4-29 16:28:33
 * @param:
 **/
public interface Checkable {

    /**
     * 入参校验
     * - 由于所有外部输入都是不可信任的，所以需要进行校验
     * - 不通过则抛出InvalidArgumentException异常
     */
    void checkInput();

}
