package com.ego.common.base;

/**
 * @description 入参接口
 *  * * * - 所有入参都必须实现此接口
 *  * * * - 请求（Request）作为顶层入参，可以包含任意下级入参参数（Param），参数（Param）可包含其他参数（Param），如下所示
 *  * * * -   Request
 *  * * * -     Param
 *  * * * -       Param <br> <br>
 * @author lyy
 * @date 2021-4-29 16:29:13
 * @param:
 **/
public interface Input extends Transferable, Checkable {
}
