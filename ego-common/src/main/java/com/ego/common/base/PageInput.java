package com.ego.common.base;

import com.ego.common.util.AbstractParamUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author lyy
 * @description: 分页方式 <br>
 * @date 2021-4-29 16:30:01
 * @param:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "分页方式")
final public class PageInput implements Input {
    public static final int DEFAULT_PAGE_NO = 1;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int MIN_PAGE_SIZE = 1;
    public static final int MAX_PAGE_SIZE = 1024;
    private static final long serialVersionUID = -98424552L;

    @ApiModelProperty(value = "请求页码-默认1，从1开始", required = true)
    private int pageNo = DEFAULT_PAGE_NO;

    @ApiModelProperty(value = "每页条数-默认20，最大1024", required = true)
    private int pageSize = DEFAULT_PAGE_SIZE;

    @Override
    public void checkInput() {
//        super.checkInput();

        AbstractParamUtil.expectTrue(pageNo >= DEFAULT_PAGE_NO,
                String.format("请求页码不能小于%d", DEFAULT_PAGE_NO));

        AbstractParamUtil.expectInRange(pageSize, MIN_PAGE_SIZE, MAX_PAGE_SIZE,
                String.format("每页条数需在[%d, %d]范围内", MIN_PAGE_SIZE, MAX_PAGE_SIZE));

    }
}
