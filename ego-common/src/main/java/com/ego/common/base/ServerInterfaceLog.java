package com.ego.common.base;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 服务端记录的接口日志
 * @author lyy
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServerInterfaceLog extends AbstractInterfaceLog {
    private static final long serialVersionUID = 8334461696591536552L;

    /**
     * 服务端IP地址
     */
    private String serverIp;

    /**
     * 服务端类型
     */
    private String serverType;

    @Override
    public boolean isServer() {
        return true;
    }
}
