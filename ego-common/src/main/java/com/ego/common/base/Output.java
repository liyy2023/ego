package com.ego.common.base;

/**
 * @description: 出参接口
 * * - 所有出参都必须实现此接口
 * * - 响应（Response）作为顶层出参，可以包含下级出参信息（Info），信息（Info）可以包含其他信息（Info），如下所示
 * * -   Response<Info>
 * * -     Info
 * * -       Info <br>
 *@author lyy
 *@date 2021-4-29 16:29:13
 * @param:
 */
public interface Output extends Transferable {
}
