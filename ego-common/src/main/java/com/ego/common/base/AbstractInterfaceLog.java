package com.ego.common.base;


import com.ego.common.constant.InterfaceType;
import com.ego.common.constant.ResultType;
import com.ego.common.util.AbstractJsonUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

/**
 * 接口日志抽象类
 * @author lyy
 * @date 2021-4-29 16:27:23
 */
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
public abstract class AbstractInterfaceLog implements Transferable {
    private static final long serialVersionUID = 8334461696591536552L;
    /**
     * 日志编号
     */
    private Long id;
    /**
     * 请求消息唯一标识
     */
    private String uuid;
    /**
     * 开始时间（精确到毫秒）
     */
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.S", timezone = "GMT+8")
    private Date startTime;
    /**
     * 结束时间（精确到毫秒）
     */
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.S", timezone = "GMT+8")
    private Date endTime;
    /**
     * 执行耗时（毫秒）
     */
    private long costMillis;
    /**
     * 客户端IP地址
     */
    private String clientIp;
    /**
     * 客户端类型
     */
    private String clientType;
    /**
     * 接口类型
     */
    private InterfaceType type;
    /**
     * 结果类型
     */
    private ResultType result;
    /**
     * 是否为写操作接口
     */
    private boolean isWrite;
    /**
     * 请求类名，为每个请求需要定义唯一类名
     */
    private String requestClass;
    /**
     * 请求消息（客户端生成）
     */
    private String request;
    /**
     * 应答消息（服务端生成）
     */
    private String response;
    /**
     * 异常信息
     */
    private String errorStack;
    /**
     * 额外信息，设Map值为String以便序列化
     */
    private Map<String, String> extra;

    /**
     * @return true-是服务端，false-是客户端
     */
    public abstract boolean isServer();

    /**
     * 计算耗时
     */
    public void calcCostMillis() {
        endTime = new Date();
        this.costMillis = endTime.getTime() - startTime.getTime();
    }

    @Override
    public String toString() {
        return AbstractJsonUtil.getIndentJsonString(this);
    }
}
