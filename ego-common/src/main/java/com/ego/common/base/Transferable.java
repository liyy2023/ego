package com.ego.common.base;


import com.ego.common.util.AbstractJsonUtil;

import java.io.Serializable;

/**
 * @author lyy
 * @description: 数据传输接口
 * * - 所有接口入参和出参必须实现此接口 <br>
 * @date 2021-4-29 16:31:04
 * @param:
 */
public interface Transferable extends Serializable {

    /**
     * @return 不含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    default String excludeEmptyFieldsJson() {
        return AbstractJsonUtil.getIndentNonEmptyJsonString(this);
    }

    /**
     * @return 包含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    default String includeEmptyFieldsJson() {
        return AbstractJsonUtil.getIndentJsonString(this);
    }
}
