package com.ego.common.base;

/**
 * @description: 写请求接口
 * * - 所有增删改接口请求必须实现此接口 <br>
 * @author lyy
 * @date 2021-4-29 16:31:18
 * @param:
 */
public interface Writeable {
}
