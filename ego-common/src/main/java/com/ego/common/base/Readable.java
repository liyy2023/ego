package com.ego.common.base;

/**
 * @description: 读请求接口
 * * - 所有查询接口请求必须实现此接口 <br>
 * @author lyy
 * @date 2021-4-29 16:30:29
 * @param:
 */
public interface Readable {
}
