package com.ego.common.response;

import com.ego.common.base.Output;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * @description: 分页返回结果 <br>
 * @author lyy
 * @date 2021-4-29 16:46:00
 * @param:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "分页结果")
final public class PageResponse<T> implements Output {
    private static final long serialVersionUID = -8832496L;

    @ApiModelProperty(value = "数据总条数")
    private Long total;

    @ApiModelProperty(value = "分页结果数据列表")
    private List<T> data;

    public static <T> PageResponse<T> empty(Class<T> clazz) {
        List<T> emptyList = Collections.emptyList();
        return new PageResponse<>(0L, emptyList);
    }

    public static <T> PageResponse<T> empty() {
        return new PageResponse<>(0L, Collections.emptyList());
    }

    public boolean isEmpty() {
        return Objects.equals(0L, total) || data == null || data.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageResponse<?> pageInfo = (PageResponse<?>) o;
        return Objects.equals(total, pageInfo.total) &&
                Objects.equals(data, pageInfo.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(total, data);
    }

    @Override
    public String toString() {
        return excludeEmptyFieldsJson();
    }

}
