package com.ego.common.response;

import com.ego.common.base.Output;
import com.ego.common.constant.CommonResponseEnum;
import com.ego.common.constant.ResponseCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: REST接口应答 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:46:13 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "REST接口应答")
final public class RestResponse<T> implements Output {
    private static final long serialVersionUID = -2349476930L;

    @ApiModelProperty(value = "是否业务成功", required = true)
    private boolean success;

    @ApiModelProperty(value = "应答数据，若业务失败则为null")
    private T data;

    @ApiModelProperty(value = "应答编码，若业务成功则返回99990000", required = true)
    private String code;

    @ApiModelProperty(value = "应答消息，根据应答编码设置", required = true)
    private String message;

    /**
     * 成功后返回数据和通用message
     * 使用场景：建议写操作成功后使用
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> RestResponse<T> ok(T data) {
        RestResponse<T> resp = new RestResponse<>();
        resp.success = true;
        resp.data = data;
        resp.code = CommonResponseEnum.Success.getCode();
        resp.message = CommonResponseEnum.Success.getMessage();
        return resp;
    }

    /**
     * 成功后返回数据和自定义message
     * 使用场景：业务有自定义成功消息的场景
     *
     * @param data 返回的数据
     * @param code 成功时需要自定义展示的消息
     * @param <T>
     * @return
     */
    public static <T> RestResponse<T> ok(T data, ResponseCode code) {
        RestResponse resp = ok(data);
        if (null != code) {
            resp.setMessage(code.getMessage());
        }
        return resp;
    }

    /**
     * 仅返回成功的消息，不返回数据
     * @param code 自定义Code
     * @param <T>
     * @return
     */
    public static <T> RestResponse<T> ok(ResponseCode code) {
        return ok(null, code);
    }


    /**
     * 使用场景：失败后返回自定义信息
     *
     * @param code
     * @param <T>
     * @return
     */
    public static <T> RestResponse<T> fail(ResponseCode code) {
        return fail(code.getCode(), code.getMessage());
    }

    public static <T> RestResponse<T> fail(String code, String message) {
        RestResponse<T> resp = new RestResponse<>();
        resp.success = false;
        resp.data = null;
        resp.code = code;
        resp.message = message;
        return resp;
    }

    @Override
    public String toString() {
        return includeEmptyFieldsJson();
    }

}
