package com.ego.common.response;

import com.ego.common.base.Output;
import com.ego.common.constant.FailInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: RPC接口应答 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:46:41 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "RPC接口应答")
final public class RpcResponse<T> implements Output {

    private static final long serialVersionUID = -2349476930L;

    @ApiModelProperty(value = "是否业务成功", required = true)
    private boolean success;

    @ApiModelProperty(value = "业务成功数据：成功时返回（若无需返回数据则为null）")
    private T data;

    @ApiModelProperty(value = "业务失败详情：失败时返回")
    private FailInfo failInfo;

    public static <T> RpcResponse<T> ok(T data) {
        RpcResponse<T> resp = new RpcResponse<>();
        resp.success = true;
        resp.data = data;
        return resp;
    }

    public static <T> RpcResponse<T> fail(String code, String message) {
        RpcResponse<T> resp = new RpcResponse<>();
        resp.success = false;
        resp.failInfo = FailInfo.builder()
                .code(code)
                .message(message)
                .build();
        return resp;
    }

    public static <T> RpcResponse<T> fail(FailInfo failInfo) {
        RpcResponse<T> resp = new RpcResponse<>();
        resp.success = false;
        resp.failInfo = failInfo;
        return resp;
    }

    @Override
    public String toString() {
        return excludeEmptyFieldsJson();
    }

}
