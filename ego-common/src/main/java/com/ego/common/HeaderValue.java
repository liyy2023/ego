package com.ego.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识数据从header中取得，如果header中没有该数据，将不会覆盖原值
 * @author lyy
 * @description
 * @date 2021-4-29 16:25:11
 **/
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HeaderValue {

    String value() default "";

}
