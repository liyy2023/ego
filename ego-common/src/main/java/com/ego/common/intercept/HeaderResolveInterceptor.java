package com.ego.common.intercept;

import com.ego.common.request.RequestContext;
import com.google.common.collect.Maps;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Map;

/**
 * 在rest层写配置添加拦截器
 * @author lyy
 * @description
 * @date 2021-4-29 16:43:03
 **/
public class HeaderResolveInterceptor implements HandlerInterceptor {

    private Boolean isWriteHeader;

    public HeaderResolveInterceptor(){
        this.isWriteHeader = Boolean.TRUE;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Enumeration<String> keys = request.getHeaderNames();
        Map<String, String> params = Maps.newHashMap();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            params.put(key, request.getHeader(key));
        }
        params.put("iswriteheader",isWriteHeader.toString());
        RequestContext.setHeaderParams(params);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        RequestContext.removeHeaderParams();
    }
}
