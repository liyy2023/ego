package com.ego.common.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: RPC接口请求基类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:45:05 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRpcRequest extends AbstractRequest {
    private static final long serialVersionUID = -677923676L;

    @Override
    public void checkInput() {
        super.checkInput();
    }

    @Override
    public boolean isWrite() {
        return false;
    }
}
