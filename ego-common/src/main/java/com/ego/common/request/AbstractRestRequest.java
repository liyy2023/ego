package com.ego.common.request;

import com.ego.common.constant.ClientInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 * @description: REST接口请求基类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:44:51 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor

public abstract class AbstractRestRequest extends AbstractRequest {
    private static final long serialVersionUID = -677923676L;

    @ApiModelProperty(hidden = true, value = "客户端")
    private ClientInfo clientInfo;

    @Override
    public boolean isWrite() {
        return false;
    }

    @Override
    public void checkInput() {
        super.checkInput();

        if (!Objects.isNull(clientInfo)) {
            clientInfo.checkInput();
        }

    }
}
