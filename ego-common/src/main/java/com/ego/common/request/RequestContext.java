/**
 * cn.com.ocj.giant
 * Copyright(c) 2012-2019 All Rights Reserved.
 */
package com.ego.common.request;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Map;

/**
 * Request上下文
 *
 * @author lyy
 * @date 2021-4-29 16:45:30
 */
public class RequestContext {
    /**
     * header参数
     */
    private static ThreadLocal<Map<String, String>> headerParams = new ThreadLocal<>();

    /**
     * 在ThreadLocal中设置当前header参数
     *
     * @param params
     */
    public static void setHeaderParams(Map<String, String> params) {
        if (!CollectionUtils.isEmpty(params)) {
            headerParams.set(params);
        }
    }

    /**
     * 从ThreadLocal中获取header参数集合
     *
     * @return
     */
    public static Map<String, String> getHeaderParams() {
        Map<String, String> params = headerParams.get();
        if (params == null) {
            params = Collections.emptyMap();
        }
        return params;
    }

    /**
     * 从ThreadLocal中获取指定的参数
     *
     * @param key 参数key值
     * @return
     */
    public static String getHeaderParam(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }

        Map<String, String> params = headerParams.get();
        if (params != null) {
            return params.get(key.toLowerCase());
        }
        return null;
    }

    /**
     * 删除ThreadLocal中当前header信息
     */
    public static void removeHeaderParams() {
        headerParams.remove();
    }
}
