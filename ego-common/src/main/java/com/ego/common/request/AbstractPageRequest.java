package com.ego.common.request;

import com.ego.common.base.PageInput;
import com.ego.common.util.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: RPC分页查询请求基类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:43:28 <br>
 * @author: lyy <br>
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractPageRequest extends AbstractRequest {
    private static final long serialVersionUID = -98424552L;

    @ApiModelProperty(value = "分页方式")
    private PageInput pageType;

    @Override
    public boolean isWrite() {
        return false;
    }

    @Override
    public void checkInput() {
        super.checkInput();
        AbstractParamUtil.nonNull(pageType, "分页方式不能为空");
        pageType.checkInput();
    }
}
