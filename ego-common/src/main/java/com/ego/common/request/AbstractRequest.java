package com.ego.common.request;

import com.ego.common.HeaderValue;
import com.ego.common.base.Input;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @description 所有接口请求基类
 *  *  * - REST接口请求基类{@link AbstractRestRequest}
 *  *  * - RPC接口请求基类{@link AbstractRpcRequest} <br>
 * @author lyy
 * @date 2021-4-29 16:44:38
 * @param:
 **/
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRequest implements Input {
    private static final long serialVersionUID = -677923676L;

    @ApiModelProperty(value = "请求消息唯一标识，由前端或API网关生成以便实现跟踪和幂等")
    private String uuid;

//    @ApiModelProperty(value = "额外信息，设置Map的值为String类型以便序列化")
//    private Map<String, String> extra;
    @ApiModelProperty(hidden = true, value = "客户端类型:1-app;2-web;3-pc", required = true)
    @HeaderValue
    private String clientType;

    @ApiModelProperty(hidden = true, value = "客户端IP", required = true)
    @HeaderValue
    private String clientIp;

    @ApiModelProperty(hidden = true, value = "用户类型1-普通用户;2-租户员工；3-后台管理用户；4-游客", required = false)
    @HeaderValue
    private String userType;

    @ApiModelProperty(hidden = true, value = "用户编号", required = false)
    @HeaderValue
    private String userId;

    @ApiModelProperty(hidden = true, value = "登录账号，登录时所使用的账号名")
    @HeaderValue
    private String account;

    @ApiModelProperty(hidden = true, value = "请求类名，每个请求需要定义一个唯一的类名")
    public String getClassName() {
        return getClass().getName();
    }

    @ApiModelProperty(hidden = true, value = "是否为写请求，用于区分CQRS读写操作")
    public abstract boolean isWrite();

    @Override
    public String toString() {
        return excludeEmptyFieldsJson();
    }

    @Override
    public void checkInput() {
//        AbstractParamUtil.notBlank(clientType,"客户端类型不能为空");
//        AbstractParamUtil.notBlank(clientIp,"客户端IP不能为空");
//        AbstractParamUtil.notBlank(userType,"用户类型不能为空");
//        // 如果不是游客用户类型则需要传入用户id和用户账号信息
//        if(!UserTypeEnum.TOURIST.getCode().equals(userType)){
//            AbstractParamUtil.notBlank(userId,"用户id不能为空");
//            AbstractParamUtil.notBlank(account,"用户账号不能为空");
//        }
    }
}
