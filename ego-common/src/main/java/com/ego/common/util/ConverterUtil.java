package com.ego.common.util;

import lombok.extern.slf4j.Slf4j;


/**
 * @description: 数据转换工具 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:52:02 <br>
 * @author: lyy <br>
 */
@Slf4j
public class ConverterUtil {
    /**
     * 将String类型数据转换为指定的类型
     *
     * @param value
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T converter(String value, Class<T> clazz) {
        try {
            if (clazz == int.class || clazz == Integer.class) {
                return (T) Integer.valueOf(value);
            } else if (clazz == long.class || clazz == Long.class) {
                // long型值有点特殊，通过ocj-middleware-tool写到redis map中的数据是：["java.lang.Long",10000000000]
                return (T) parseLong(value);
            } else if (clazz == boolean.class || clazz == Boolean.class) {
                return (T) Boolean.valueOf(value);
            } else if (clazz == short.class || clazz == Short.class) {
                return (T) Short.valueOf(value);
            } else if (clazz == double.class || clazz == Double.class) {
                return (T) Double.valueOf(value);
            } else if (clazz == float.class || clazz == Float.class) {
                return (T) Float.valueOf(value);
            } else if (clazz == byte.class || clazz == Byte.class) {
                return (T) Byte.valueOf(value);
            } else if (clazz == char.class || clazz == Character.class) {
                return (T) Character.valueOf(value.charAt(0));
            } else {
                log.warn("Converter failed. cause unSupport class: {}", clazz.getName());
            }
        } catch (Exception ex) {
            log.error("Converter failed. value: {}, to: {}, cause: {}", value, clazz.getName(), ex.getMessage());
        }
        return null;
    }

    /**
     * long型值有点特殊，通过ocj-middleware-tool写到redis map中的数据是：["java.lang.Long",10000000000]
     *
     * @param value
     * @return
     */
    protected static Long parseLong(String value) {
        try {
            boolean isAllDigit = true;
            char[] chars = value.toCharArray();
            for (char ch : chars) {
                if (!Character.isDigit(ch)) {
                    isAllDigit = false;
                    break;
                }
            }

            String newValue = value;
            if (!isAllDigit && chars[0] == '[' && chars[chars.length - 1] == ']') {
                int index = -1;
                for (int i = 0, s = chars.length; i < s; i++) {
                    if (chars[i] == ',') {
                        index = i;
                        break;
                    }
                }
                newValue = value.substring(index + 1, chars.length - 1);
            }

            return Long.valueOf(newValue);
        } catch (NumberFormatException ex) {
            log.error("Converter failed. cause: NumberFormatException: {}", value);
        } catch (IndexOutOfBoundsException ex) {
            log.error("Converter failed. cause: IndexOutOfBoundsException: {}", value);
        }
        return null;
    }
}
