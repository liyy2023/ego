package com.ego.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @description: IP工具类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:47:44 <br>
 * @author: lyy <br>
 */
public abstract class AbstractIpUtil {
    private static final String LOCAL_IP_ADDRESS = "127.0.0.1";

    /**
     * 获取本机IP
     *
     * @return 本机IP
     */
    public static String getLocalIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return LOCAL_IP_ADDRESS;
        }
    }
}
