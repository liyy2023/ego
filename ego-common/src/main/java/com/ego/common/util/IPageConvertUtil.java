package com.ego.common.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ego.common.request.AbstractPageRequest;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author lyy
 * @date 2021-4-29 17:38:20
 */
public class IPageConvertUtil {
     public static <T,E> IPage<T> restToDtaIPage(IPage<E> iPage){
        IPage<T> dtoIPage=new Page<>();
        dtoIPage.setPages(iPage.getPages());
        dtoIPage.setTotal(iPage.getTotal());
        dtoIPage.setCurrent(iPage.getCurrent());
        dtoIPage.setSize(iPage.getSize());
        return dtoIPage;
    }
    public static <T> IPage<T> setPage(AbstractPageRequest request){
        IPage<T> iPage=new Page<>();
        iPage.setCurrent(request.getPageType().getPageNo());
        iPage.setSize(request.getPageType().getPageSize());
        return iPage;
    }
}
