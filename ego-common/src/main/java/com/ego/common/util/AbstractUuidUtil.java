package com.ego.common.util;

import java.util.UUID;

/**
 * @description: UUID 工具类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:51:45 <br>
 * @author: lyy <br>
 */
public abstract class AbstractUuidUtil {

    public static String uuidWithoutUnderscore() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
