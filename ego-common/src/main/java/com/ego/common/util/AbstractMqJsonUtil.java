package com.ego.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.datatype.guava.GuavaModule;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.util.Objects;

/**
 * @description: 消息工具类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:49:09 <br>
 * @author: lyy <br>
 * @param
 * @return 
 */ 
public abstract class AbstractMqJsonUtil {

    /**
     * MQ消息：自动缩进且过滤掉空属性的JSON-Object转换器，支持范型序列化和反序列化
     */
    private static final ObjectMapper INDENT_OBJECT_MAPPER_NON_EMPTY_MQ = new ObjectMapper();

    static {
        INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.enable(SerializationFeature.INDENT_OUTPUT);
        INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.registerModule(new GuavaModule());
        initTypeInclusion();
    }

    private static void initTypeInclusion() {
        TypeResolverBuilder<?> mapTyper = new ObjectMapper.DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.NON_FINAL) {
            @Override
            public boolean useForType(JavaType t) {
                switch (_appliesFor) {
                    case NON_CONCRETE_AND_ARRAYS:
                        while (t.isArrayType()) {
                            t = t.getContentType();
                        }
                        // fall through
                    case OBJECT_AND_NON_CONCRETE:
                        return (t.getRawClass() == Object.class) || !t.isConcrete();
                    case NON_FINAL:
                        while (t.isArrayType()) {
                            t = t.getContentType();
                        }
//                        // to fix problem with wrong long to int conversion
//                        if (t.getRawClass() == Long.class) {
//                            return true;
//                        }
                        if (t.getRawClass() == XMLGregorianCalendar.class) {
                            return false;
                        }
                        return !t.isFinal(); // includes Object.class
                    default:
                        // case JAVA_LANG_OBJECT:
                        return t.getRawClass() == Object.class;
                }
            }
        };
        mapTyper.init(JsonTypeInfo.Id.CLASS, null);
        mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
        AbstractMqJsonUtil.INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.setDefaultTyping(mapTyper);

        // warm up codec
        try {
            byte[] s = AbstractMqJsonUtil.INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.writeValueAsBytes(1);
            AbstractMqJsonUtil.INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.readValue(s, Object.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String getMqJsonString(Object o) {
        if (Objects.isNull(o)) {
            return "";
        }
        try {
            return INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.writeValueAsString(o);
        } catch (Exception e) {
            return doToStringException(o, e);
        }
    }

    public static <T> T getMqObject(String jsonSting, Class<T> beanClass) {
        if (Objects.nonNull(jsonSting) && Objects.nonNull(beanClass)) {
            try {
                return INDENT_OBJECT_MAPPER_NON_EMPTY_MQ.readValue(jsonSting, beanClass);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private static String doToStringException(Object o, Exception e) {
        return "[WARN][Object toString() cause Jackson Serialization Failed] [" + o.getClass().getName() + "] " + e.getMessage();
    }
}
