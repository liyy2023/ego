package com.ego.common.util;


import com.ego.common.exception.GlobalInvalidArgumentException;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;

/**
 * @description: 校验抽象类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:49:58 <br>
 * @author: lyy <br>
 */ 
public abstract class AbstractParamUtil {

    /**
     * 断言字符串空
     * 该断言会会判断字符串是否为空，只包含空白符也会判断为空
     *
     * @param string 断言字符串
     * @param msg    提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void isBlank(String string, String msg) {
        if (AbstractStringUtil.isNotBlank(string)) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 断言字符串非空
     * 该断言会会判断字符串是否为空，只包含空白符也会判断为空
     *
     * @param string 断言字符串
     * @param msg    提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void notBlank(String string, String msg) {
        if (AbstractStringUtil.isBlank(string)) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 断言集合非空
     *
     * @param collection 集合
     * @param msg        提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void notEmpty(Collection collection, String msg) {
        if (null == collection || collection.isEmpty()) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 断言String集合非空和集合元素非空
     *
     * @param collection String集合
     * @param elementMsg 集合元素不符合要求提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void nonBlankElements(Collection<String> collection, String elementMsg) {
        for (String str : collection) {
            notBlank(str, elementMsg);
        }
    }

    /**
     * 断言对象非null
     *
     * @param object 对象
     * @param msg    提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void nonNull(Object object, String msg) {
        if (null == object) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 断言对象null
     *
     * @param object 对象
     * @param msg    提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void isNull(Object object, String msg) {
        if (null != object) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 断言表达式结果为真,为假则提示异常信息
     *
     * @param boolExpression 布尔表达式
     * @param falseMsg       提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void expectTrue(boolean boolExpression, String falseMsg) {
        if (!boolExpression) {
            throw new GlobalInvalidArgumentException(falseMsg);
        }
    }

    /**
     * 断言表达式结果为假,为真则提示异常信息
     *
     * @param boolExpression 布尔表达式
     * @param trueMsg        提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void expectFalse(boolean boolExpression, String trueMsg) {
        if (boolExpression) {
            throw new GlobalInvalidArgumentException(trueMsg);
        }
    }

    /**
     * 断言表达式结果都为真，抛出提示信息
     *
     * @param booleans 布尔表达式数组
     * @param msg      提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void expectAnyFalse(String msg, Boolean... booleans) throws GlobalInvalidArgumentException {
        if (Arrays.stream(booleans).allMatch(t -> t)) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 检查集合元素个数在[min, max]区间
     *
     * @param collection  要断言的集合
     * @param minElements 集合元素个数最小值
     * @param maxElements 集合元素个数最大值
     * @param msg         条件表达式为true的提示消息
     */
    public static void expectInRange(Collection collection, int minElements, int maxElements, String msg) {
        expectInRange(collection.size(), minElements, maxElements, msg);
    }

    /**
     * 检查字符串长度在[min, max]区间
     *
     * @param string 要断言的字符串
     * @param msg    提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void expectInRange(String string, int minLength, int maxLength, String msg) {
        if (AbstractStringUtil.isBlank(string) || string.length() < minLength || string.length() > maxLength) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 检查整数值在[min, max]区间
     *
     * @param value    要断言的整数值
     * @param minValue 允许的最小值
     * @param maxValue 允许的最大值
     * @param msg      提示信息
     */
    public static void expectInRange(int value, int minValue, int maxValue, String msg) {
        if (value < minValue || value > maxValue) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }

    /**
     * 检查日期格式
     *
     * @param sDate   断言日期字符串
     * @param pattern 日期格式字符串
     * @param msg     提示信息
     * @throws GlobalInvalidArgumentException 异常
     */
    public static void expectDateStrWithPattern(String sDate, String pattern, String msg) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            df.parse(sDate);
        } catch (Exception e) {
            throw new GlobalInvalidArgumentException(msg);
        }
    }
}
