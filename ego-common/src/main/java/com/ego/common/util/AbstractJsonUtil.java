package com.ego.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Objects;

/**
 * @description: 工具类 <br>
 * @version: 1.0 <br>
 * @date: 2021-4-29 16:48:19 <br>
 * @author: lyy <br>
 */
public abstract class AbstractJsonUtil {

    /**
     * 自动缩进且过滤掉空属性的JSON-Object转换器
     */
    private static final ObjectMapper INDENT_OBJECT_MAPPER_NON_EMPTY = new ObjectMapper();

    /**
     * 自动缩进的JSON-Object转换器
     */
    private static final ObjectMapper INDENT_OBJECT_MAPPER = new ObjectMapper();

    /**
     * 不自动缩进的JSON-Object转换器
     */
    private static final ObjectMapper NON_INDENT_OBJECT_MAPPER = new ObjectMapper();

    /**
     * 将对象所有属性/字段转换成Map: key=String, value=Object的JSON-Object转换器
     */
    private static final ObjectMapper OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER = new ObjectMapper();

    static {
        INDENT_OBJECT_MAPPER_NON_EMPTY.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        INDENT_OBJECT_MAPPER_NON_EMPTY.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        INDENT_OBJECT_MAPPER_NON_EMPTY.enable(SerializationFeature.INDENT_OUTPUT);
        INDENT_OBJECT_MAPPER_NON_EMPTY.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        INDENT_OBJECT_MAPPER_NON_EMPTY.registerModule(new GuavaModule());

        INDENT_OBJECT_MAPPER.registerModule(new JavaTimeModule());
        INDENT_OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        INDENT_OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        INDENT_OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);

        NON_INDENT_OBJECT_MAPPER.registerModule(new JavaTimeModule());
        NON_INDENT_OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        NON_INDENT_OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER.registerModule(new GuavaModule());
        OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * @return 不含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    public static String getIndentNonEmptyJsonString(Object o) {
        if (Objects.isNull(o)) {
            return "";
        }
        try {
            return INDENT_OBJECT_MAPPER_NON_EMPTY.writeValueAsString(o);
        } catch (Exception e) {
            return doToStringException(o, e);
        }
    }

    /**
     * @return 包含取值为null、空字符串、空集合的字段且带缩进的JSON字符串
     */
    public static String getIndentJsonString(Object o) {
        if (Objects.isNull(o)) {
            return "";
        }
        try {
            return INDENT_OBJECT_MAPPER.writeValueAsString(o);
        } catch (Exception e) {
            return doToStringException(o, e);
        }
    }

    public static String getNonIndentJsonString(Object o) {
        if (Objects.isNull(o)) {
            return "";
        }
        try {
            return NON_INDENT_OBJECT_MAPPER.writeValueAsString(o);
        } catch (Exception e) {
            return doToStringException(o, e);
        }
    }

    public static <T> T getObject(String jsonSting, Class<T> beanClass) {
        if (Objects.nonNull(jsonSting) && Objects.nonNull(beanClass)) {
            try {
                return INDENT_OBJECT_MAPPER.readValue(jsonSting, beanClass);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> objectFieldsToStringObjectMap(Object obj) {
        return (Map<String, Object>) OBJECT_FIELDS_TO_STRING_OBJECT_MAP_MAPPER.convertValue(obj, Map.class);
    }

    private static String doToStringException(Object o, Exception e) {
        return "[WARN][Object toString() cause Jackson Serialization Failed] [" + o.getClass().getName() + "] " + e.getMessage();
    }
}
