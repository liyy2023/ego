package com.ego.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;
import java.util.Map;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:53:23
 **/
public class TokenUtil {


    /**
     * @param secret 生成token的秘钥
     * @param expireDate 过期时间，单位秒
     * @param claim 额外参数
     * @return
     */
    public static String creatToken(String secret,Integer expireDate, Map<String, String> claim){
        try{
            //secret 密钥
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String token = "";
            JWTCreator.Builder builder = JWT.create();
            // 发布者
            builder.withIssuer("auth0");
            // 生成签名的时间
            builder.withIssuedAt(new Date());
            // 生成签名的有效期,小时
            builder.withExpiresAt(DateUtils.addSeconds(new Date(), expireDate));
            // 插入数据
            claim.forEach(builder::withClaim);
            token = builder.sign(algorithm);
            return token;
        }catch(JWTCreationException e){
            e.printStackTrace();
            //如果Claim不能转换为JSON，或者在签名过程中使用的密钥无效，那么将会抛出JWTCreationException异常。
            return null;
        }
    }

    /**
     * 验证token是否过期
     * @param secret
     * @param token
     * @param claim
     * @return
     */
    public static Boolean verifierToken(String secret, String token,Map<String, String> claim){
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            Verification require = JWT.require(algorithm);
                    //匹配指定的token发布者 auth0
            require.withIssuer("auth0");
            claim.forEach(require::withClaim);
            JWTVerifier verifier = require.build();
            //解码JWT ，verifier 可复用
            DecodedJWT jwt = verifier.verify(token);
            // 获取过期时间如果大于等于当前时间则没有过期，否则已过期
            return jwt.getExpiresAt().compareTo(new Date()) >= 0;
        }catch (JWTVerificationException e){
            //过期
            return false;
        }
    }

    private static DecodedJWT decodedJWT(String token){
        return JWT.decode(token);
    }

    /**
     * 获取算法类型
     * @param token
     * @return
     */
    public static String getAlgorithm(String token){
        return TokenUtil.decodedJWT(token).getAlgorithm();
    }

    /**
     * 获取token过期时间
     * @param token
     * @return
     */
    public static Date getExpiresAt(String token){
        return TokenUtil.decodedJWT(token).getExpiresAt();
    }

    /**
     * 获取token生产日期
     * @param token
     * @return
     */
    public static Date getIssuedAt(String token){
        return TokenUtil.decodedJWT(token).getIssuedAt();
    }

    /**
     * 获取token发布者
     * @param token
     * @return
     */
    public static String getIssuer(String token){
        return TokenUtil.decodedJWT(token).getIssuer();
    }
    public static Map<String, Claim> getClaims(String token){
        Map<String, Claim> claims = TokenUtil.decodedJWT(token).getClaims();
        return claims;
    }

    /**
     * 根据key获取对应的值
     * @param token
     * @param key
     * @return
     */
    public static String getClaimByKey(String token, String key){
        Claim claim = TokenUtil.decodedJWT(token).getClaim(key);
        return claim.asString();
    }
}