package com.ego.common.util;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 文件资源 工具类
 */
public abstract class AbstractFileResourceUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger("server");
    private static final ObjectMapper OBJECT_MAPPER;

    private static final String CLASS_PATH_PREFIX = "classpath:";
    private static final String PROPERTIES_SUFFIX = ".properties";
    private static final String PROPERTIES_SEPARATOR = "_";
    private static final int PROPERTIES_SEPARATOR_COUNT = 2;
    private static final ResourcePatternResolver DEFAULT_RESOURCE_LOADER = new PathMatchingResourcePatternResolver();


    static {
        OBJECT_MAPPER = new ObjectMapper();
        // 忽略不能反序列化的字段
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    /**
     * 根据 JSON 文件生成对象
     *
     * @param filePath JSON 文件路径
     * @param beanType Bean 类型
     * @return 对象
     */
    public static <T> T loadBeanFromJsonFile(String filePath, Class<T> beanType) {
        try {
            try (
                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(
                                    Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath)))
            ) {
                return OBJECT_MAPPER.readValue(buffer, beanType);
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to deserialize {} from {}", beanType, filePath, e);
        }
        return null;
    }

    public static String loadStringFromFile(String filePath) {
        try {
            try (
                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(
                                    Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath)))
            ) {
                return buffer.lines().collect(Collectors.joining("\n"));
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to load string from {}", filePath, e);
        }
        return null;
    }

    public static List<String> messageSourceBaseNames(String dirPath) {
        try {
            if (dirPath.endsWith("/")) {
                dirPath = dirPath.substring(0, dirPath.length() - 1);
            }
            final String dirPathNotEndWithSlash = dirPath;

            Resource[] resources = DEFAULT_RESOURCE_LOADER.getResources(CLASS_PATH_PREFIX + dirPath + "/*");
            return Arrays.stream(resources)
                    .map(Resource::getFilename)
                    .filter(fileName -> fileName.contains(PROPERTIES_SUFFIX))
                    .filter(fileName -> isCountAtLeastRight(fileName, PROPERTIES_SEPARATOR, PROPERTIES_SEPARATOR_COUNT))
                    .map(fileName -> trimCountAtLeastRight(fileName, PROPERTIES_SEPARATOR, PROPERTIES_SEPARATOR_COUNT))
                    .map(fileNamePrefix -> CLASS_PATH_PREFIX + dirPathNotEndWithSlash + "/" + fileNamePrefix)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            LOGGER.warn("Failed to load messageSourceBaseNames from {}", dirPath, e);
        }
        return Collections.emptyList();
    }

    public static boolean isCountAtLeastRight(String src, String find, int countAtLeast) {
        while (countAtLeast > 0) {
            int i = src.lastIndexOf(find);
            if (i > 0) {
                countAtLeast--;
                src = src.substring(0, i);
            } else {
                return false;
            }
        }
        return true;
    }

    public static String trimCountAtLeastRight(String src, String find, int countAtLeast) {
        while (countAtLeast > 0) {
            int i = src.lastIndexOf(find);
            if (i > 0) {
                countAtLeast--;
                src = src.substring(0, i);
            } else {
                return "";
            }
        }
        return src;
    }
}
