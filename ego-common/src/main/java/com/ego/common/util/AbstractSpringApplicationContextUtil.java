package com.ego.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Spring应用上下文工具类
 */
public abstract class AbstractSpringApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext context;

    public static ApplicationContext getContext() {
        return context;
    }

    public static Object getBean(String beanName) {
        return context == null ? null : context.getBean(beanName);
    }

    public static <T> T getBean(Class<T> beanClass) {
        return context == null ? null : context.getBean(beanClass);
    }

    public static String[] getBeanDefinitionNames() {
        return context == null ? null : context.getBeanDefinitionNames();
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        AbstractSpringApplicationContextUtil.context = context;
    }
}
