package com.ego.common.util;

import java.util.Random;

/**
 * 生成验证码
 * @author lyy
 * @description
 * @date 2021-4-29 16:53:49
 **/
public class VerificationCodeUtil {

    /**
     * 生成多少位验证码
     * @param length
     * @return
     */
    public static String generatorCode(Integer length){
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            // 生成一个[0,10)
            int randomInt = random.nextInt(10);
            str.append(randomInt);
        }
        return str.toString();
    }
}
