package com.ego.common.aop;

import com.ego.common.base.ServerInterfaceLog;
import com.ego.common.constant.InterfaceType;
import com.ego.common.constant.Loggers;
import com.ego.common.constant.ResultType;
import com.ego.common.exception.EgoException;
import com.ego.common.exception.ExceptionConverter;
import com.ego.common.request.AbstractRequest;
import com.ego.common.request.AbstractRestRequest;
import com.ego.common.request.AbstractRpcRequest;
import com.ego.common.request.RequestContext;
import com.ego.common.response.RestResponse;
import com.ego.common.response.RpcResponse;
import com.ego.common.util.AbstractIpUtil;
import com.ego.common.util.AbstractJsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Date;

/**
 * 开放接口（RPC或REST API） 服务端统一处理切面
 * - 入参校验：接口请求入参校验
 * - 日志记录：成功或失败
 * - 异常转换：Throwable -> EduException
 *
 * 使用：子类增加如下注解:
       @Aspect
       @Component
       @Order(0)
 * @author lyy
 * @date 2021-4-29 16:25:55
 **/
public abstract class AbstractOpenApiAop {

    protected ExceptionConverter exceptionConverter = new ExceptionConverter();
    /**
     * 切点：接口
     * - 由子类覆盖，定义自己的接口实现类所在包路径，实现方式举例：
     * - @Override
     * - @Pointcut(value = "within(com.ego..*)")
     * - public Object apiPointcut() {
     * - }
     */
    public abstract void apiPointcut();


    public Object doApi(ProceedingJoinPoint joinPoint) {
        Date startTime = new Date();

        AbstractRequest request = (AbstractRequest) joinPoint.getArgs()[0];

        try {
            //写入参数
            this.writeHeaderValue(request);

            // 接口入参校验
            request.checkInput();

            // 接口业务处理
            Object response = joinPoint.proceed();

            // 成功日志
            okLog(startTime, response, request);
            // 成功应答
            return response;
        } catch (Throwable e) {
            // 异常转换
            EgoException eduException = exceptionConverter.toEduException(e);

            // 失败日志
            failLog(startTime, eduException, request);

            // 失败应答
            return failWithCodeCheck(request, eduException);

        }
    }


    /**
     * 异常失败的返回失败报文
     * @param request
     * @param egoException
     * @return
     */
    private Object failWithCodeCheck (AbstractRequest request, EgoException egoException) {

        if (request instanceof AbstractRequest) {
            // REST接口
            return RestResponse.fail(egoException.getResponseCodeEnum().getCode(),
                    egoException.getResponseCodeEnum().getMessage());
        } else {
            // RPC接口
            return RpcResponse.fail(egoException.getResponseCodeEnum().getCode(),
                    egoException.getResponseCodeEnum().getMessage());
        }
    }

    private void okLog(Date startTime, Object response, AbstractRequest request) {
        log(startTime, null, response, request);
    }

    private void failLog(Date startTime, EgoException e, AbstractRequest request) {
        log(startTime, e, null, request);
    }

    private void log(Date startTime, EgoException e, Object response, AbstractRequest request) {
        Date endTime = new Date();

        ServerInterfaceLog serverInterfaceLog = null;
        try {
            serverInterfaceLog = new ServerInterfaceLog();
            serverInterfaceLog.setStartTime(startTime);
            serverInterfaceLog.setUuid(request.getUuid());

            if (request instanceof AbstractRestRequest) {
                AbstractRestRequest client = ((AbstractRestRequest) request);
                if(StringUtils.isNotEmpty(client.getClientIp())){
                    serverInterfaceLog.setClientIp(client.getClientIp());
                }
                if(StringUtils.isNotEmpty(client.getClientType())){
                    serverInterfaceLog.setClientType(client.getClientType());
                }
                serverInterfaceLog.setType(InterfaceType.REST_CALL);
            } else if (request instanceof AbstractRpcRequest) {
                serverInterfaceLog.setType(InterfaceType.DUBBO_CALL);
            }

            serverInterfaceLog.setServerIp(AbstractIpUtil.getLocalIp());
//            serverInterfaceLog.setServerType(env.getAppName());
            serverInterfaceLog.setWrite(request.isWrite());
            serverInterfaceLog.setRequestClass(request.getClassName());
            serverInterfaceLog.setRequest(AbstractJsonUtil.getNonIndentJsonString(request));

            // 由子类为 serverInterfaceLog 更新相关字段
            updateInterfaceLog(serverInterfaceLog, request);

            if (e != null) {
                // 操作失败
                serverInterfaceLog.setResult(ResultType.FAIL);
                serverInterfaceLog.setErrorStack(e.toString());
            } else {
                //操作成功
                serverInterfaceLog.setResult(ResultType.OK);
                serverInterfaceLog.setResponse(AbstractJsonUtil.getNonIndentJsonString(response));
            }

            // 由子类根据 serverInterfaceLog 执行相关操作
            doInterfaceLog(serverInterfaceLog, request);

            serverInterfaceLog.calcCostMillis();

        } catch (Exception e1) {
            Loggers.Monitor.error("AOP open api log fail\nserverInterfaceLog=" + serverInterfaceLog, e1);
        }
    }

    /**
     * 由子类为 serverInterfaceLog 更新相关字段
     *
     * @param serverInterfaceLog 接口日志
     * @param request            接口请求
     */
    protected abstract void updateInterfaceLog(ServerInterfaceLog serverInterfaceLog, AbstractRequest request);

    /**
     * 由子类根据 serverInterfaceLog 执行相关操作
     *
     * @param serverInterfaceLog 接口日志
     * @param request            接口请求
     */
    protected void doInterfaceLog(ServerInterfaceLog serverInterfaceLog, AbstractRequest request) {
        String logMsg = "\nServerInterfaceLog:\n" + serverInterfaceLog;

        if (ResultType.OK == serverInterfaceLog.getResult()) {
            Loggers.Monitor.info(logMsg);
        } else {
            Loggers.Monitor.error(logMsg);
        }
    }

    /**
     * 请求头信息写入
     */
    private void  writeHeaderValue(AbstractRequest request){
        // 如果不写入头信息则跳过

        String isWriteHeader = RequestContext.getHeaderParam("isWriteHeader".toLowerCase());
        if(StringUtils.isNotEmpty(isWriteHeader)&&isWriteHeader.equals(Boolean.TRUE.toString())){
            String clientType = RequestContext.getHeaderParam("clientType".toLowerCase());
            String userType = RequestContext.getHeaderParam("userType".toLowerCase());
            String userId = RequestContext.getHeaderParam("userId".toLowerCase());
            String account = RequestContext.getHeaderParam("account".toLowerCase());
            request.setClientType(clientType);
            request.setUserType(userType);
            request.setUserId(userId);
            request.setAccount(account);
        }
    }
}
