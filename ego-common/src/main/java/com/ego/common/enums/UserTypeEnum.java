package com.ego.common.enums;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:38:55
 **/
public enum UserTypeEnum {
    // 1-普通用户;2-租户员工；3-后台管理用户；4-游客
    COMMON("1"),
    TENANT("2"),
    BACKGROUND("3"),
    TOURIST("4")
    ;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    UserTypeEnum(String code) {
        this.code = code;
    }
}
