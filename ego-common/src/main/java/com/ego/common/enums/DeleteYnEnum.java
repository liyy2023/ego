package com.ego.common.enums;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:38:27
 **/
public enum DeleteYnEnum {
    DELETE_N(0),
    DELETE_Y(1)
    ;
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    DeleteYnEnum(Integer code) {
        this.code = code;
    }
}
