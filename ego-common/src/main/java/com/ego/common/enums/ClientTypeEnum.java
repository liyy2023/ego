package com.ego.common.enums;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:37:55
 **/
public enum ClientTypeEnum {
    APP("1"),
    WEB("2"),
    PC("3")
    ;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    ClientTypeEnum(String code) {
        this.code = code;
    }
}
