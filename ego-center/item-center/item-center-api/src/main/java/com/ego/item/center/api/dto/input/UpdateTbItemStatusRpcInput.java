package com.ego.item.center.api.dto.input;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lyy
 */
@Data
public class UpdateTbItemStatusRpcInput {

    @ApiModelProperty(value = "商品id")
    private Long id;

    @ApiModelProperty(value = "商品状态，1-正常，2-下架，3-删除")
    private Integer status;
}
