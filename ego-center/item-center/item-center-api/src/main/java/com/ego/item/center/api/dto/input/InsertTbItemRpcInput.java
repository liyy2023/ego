package com.ego.item.center.api.dto.input;

import com.ego.common.request.AbstractRpcRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lyy
 */
@Data
public class InsertTbItemRpcInput extends AbstractRpcRequest {

    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品标题")
    private String title;

    @ApiModelProperty("商品卖点")
    private String sellPoint;

    @ApiModelProperty("商品价格")
    private Long price;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("商品条码")
    private String barcode;

    @ApiModelProperty("商品图片")
    private String image;

    @ApiModelProperty("商品描述")
    private String description;

    @ApiModelProperty("商品类目")
    private Long cid;
}
