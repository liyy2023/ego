package com.ego.item.center.api.dto.input;

import com.ego.common.request.AbstractRpcRequest;
import com.ego.common.util.AbstractParamUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lyy
 */
@Data
public class ByIdRpcInput extends AbstractRpcRequest {
    @ApiModelProperty(value = "id",required = true)
    private Long id;

    @Override
    public void checkInput() {
        super.checkInput();
        AbstractParamUtil.notBlank(id.toString(),"id不能为空");
    }
}
