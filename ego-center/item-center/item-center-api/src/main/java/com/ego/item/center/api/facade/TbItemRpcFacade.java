package com.ego.item.center.api.facade;

import com.ego.common.response.PageResponse;
import com.ego.common.response.RpcResponse;
import com.ego.item.center.api.dto.input.*;
import com.ego.item.center.api.dto.output.QueryTbItemDetailRpcOutput;
import com.ego.item.center.api.dto.output.TbItemPageListRpcOutput;

/**
 * @author lyy
 * @date 2021-4-30 10:19:16
 */
public interface TbItemRpcFacade {
    /**
     * 分页查询商品列表
     * @param rpcInput
     * @return
     */
    RpcResponse<PageResponse<TbItemPageListRpcOutput>> getTbItemPageList(TbItemPageListRpcInput rpcInput);
    /**
     * 新增编辑商品
     * @param rpcInput
     * @return
     */
    RpcResponse<Boolean> insertOrUpdateTbItem(InsertTbItemRpcInput rpcInput);
    /**
     * 修改商品状态
     * @param rpcInput
     * @return
     */
    RpcResponse<Boolean> updateTbItemStatus(UpdateTbItemStatusRpcInput rpcInput);
    /**
     * 查询商品详情
     * @param rpcInput
     * @return
     */
    RpcResponse<QueryTbItemDetailRpcOutput> queryTbItemDetail(ByIdRpcInput rpcInput);

}
