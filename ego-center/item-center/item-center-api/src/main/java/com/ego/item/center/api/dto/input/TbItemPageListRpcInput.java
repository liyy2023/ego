package com.ego.item.center.api.dto.input;

import com.ego.common.request.AbstractPageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author lyy
 */
@Data
public class TbItemPageListRpcInput extends AbstractPageRequest {
    @ApiModelProperty(value = "商品标题")
    private String title;

    @ApiModelProperty(value = "商品卖点")
    private String sellPoint;

    @ApiModelProperty(value = "所属类目，叶子类目")
    private Long cid;

    @ApiModelProperty(value = "商品状态，1-正常，2-下架，3-删除")
    private Integer status;
}
