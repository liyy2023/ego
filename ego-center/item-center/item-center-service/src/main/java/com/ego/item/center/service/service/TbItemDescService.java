package com.ego.item.center.service.service;

import com.ego.item.center.service.entity.TbItemDesc;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品描述表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemDescService extends IService<TbItemDesc> {

    /**
     * @param tbItemDesc
     * @return
     */
    Boolean updateByTbItemId(TbItemDesc tbItemDesc);

}
