package com.ego.item.center.service.service;

import com.ego.item.center.service.entity.TbItemParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品规则参数 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemParamService extends IService<TbItemParam> {

}
