package com.ego.item.center.service.service.impl;

import com.ego.item.center.service.entity.TbItemParamItem;
import com.ego.item.center.service.mapper.TbItemParamItemMapper;
import com.ego.item.center.service.service.TbItemParamItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品规格和商品的关系表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
@Service
public class TbItemParamItemServiceImpl extends ServiceImpl<TbItemParamItemMapper, TbItemParamItem> implements TbItemParamItemService {

}
