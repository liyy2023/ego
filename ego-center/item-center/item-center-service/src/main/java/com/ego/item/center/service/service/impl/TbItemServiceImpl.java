package com.ego.item.center.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ego.item.center.api.dto.input.TbItemPageListRpcInput;
import com.ego.item.center.api.dto.input.UpdateTbItemStatusRpcInput;
import com.ego.item.center.service.entity.TbItem;
import com.ego.item.center.service.mapper.TbItemMapper;
import com.ego.item.center.service.service.TbItemService;
import com.ego.common.util.IPageConvertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
@Service
public class TbItemServiceImpl extends ServiceImpl<TbItemMapper, TbItem> implements TbItemService {

    @Override
    public IPage<TbItem> queryTbItemPageList(TbItemPageListRpcInput rpcInput) {
        LambdaQueryWrapper<TbItem> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(rpcInput.getTitle()),TbItem::getTitle,rpcInput.getTitle());
        wrapper.like(StringUtils.isNotEmpty(rpcInput.getSellPoint()),TbItem::getSellPoint,rpcInput.getSellPoint());
        wrapper.eq(rpcInput.getStatus() != null,TbItem::getStatus,rpcInput.getStatus());
        wrapper.eq(rpcInput.getCid() != null,TbItem::getCid,rpcInput.getCid());
        IPage<TbItem> tbItemIPage = this.baseMapper.selectPage(IPageConvertUtil.setPage(rpcInput), wrapper);
        return tbItemIPage;
    }

    @Override
    public Boolean updateTbItemStatus(UpdateTbItemStatusRpcInput rpcInput) {
        LambdaQueryWrapper<TbItem> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(rpcInput.getId() != null,TbItem::getId,rpcInput.getId());
        wrapper.eq(rpcInput.getStatus() != null,TbItem::getStatus,rpcInput.getStatus());
        Integer isUpdate = this.baseMapper.update(new TbItem(),wrapper);
        if(isUpdate.equals(1)){
            return Boolean.TRUE;
        }else {
            return Boolean.FALSE;
        }
    }

    @Override
    public Long queryItemIdByTitle(String title) {
        return this.baseMapper.queryItemIdByTitle(title);
    }
}
