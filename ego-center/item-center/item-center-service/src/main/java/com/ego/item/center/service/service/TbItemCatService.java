package com.ego.item.center.service.service;

import com.ego.item.center.service.entity.TbItemCat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品类目 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemCatService extends IService<TbItemCat> {

}
