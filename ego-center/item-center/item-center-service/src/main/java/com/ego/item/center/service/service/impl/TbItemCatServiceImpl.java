package com.ego.item.center.service.service.impl;

import com.ego.item.center.service.entity.TbItemCat;
import com.ego.item.center.service.mapper.TbItemCatMapper;
import com.ego.item.center.service.service.TbItemCatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品类目 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
@Service
public class TbItemCatServiceImpl extends ServiceImpl<TbItemCatMapper, TbItemCat> implements TbItemCatService {

}
