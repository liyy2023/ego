package com.ego.item.center.service.converter.input;

import com.ego.item.center.api.dto.input.InsertTbItemRpcInput;
import com.ego.item.center.service.entity.TbItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;

/**
 * @author lyy
 */
@Mapper(componentModel = "spring",nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface TbItemConverterInput {

    /**
     * @param rpcInput
     * @return
     */
    TbItem rpcInput2TbItem(InsertTbItemRpcInput rpcInput);

}
