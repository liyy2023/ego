package com.ego.item.center.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ego.item.center.service.entity.TbItemDesc;
import com.ego.item.center.service.mapper.TbItemDescMapper;
import com.ego.item.center.service.service.TbItemDescService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品描述表 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
@Service
public class TbItemDescServiceImpl extends ServiceImpl<TbItemDescMapper, TbItemDesc> implements TbItemDescService {

    @Override
    public Boolean updateByTbItemId(TbItemDesc tbItemDesc) {
        LambdaQueryWrapper wrapper = new LambdaQueryWrapper();
//        wrapper.eq(null != tbItemDesc.getItemId(),TbItemDesc::getItemId,tbItemDesc.getItemId());
        Integer isUpdate = this.baseMapper.update(tbItemDesc,wrapper);
        if (isUpdate.equals(1)){
            return Boolean.TRUE;
        }else {
            return Boolean.FALSE;
        }
    }
}
