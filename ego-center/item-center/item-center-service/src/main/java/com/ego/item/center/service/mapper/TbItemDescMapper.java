package com.ego.item.center.service.mapper;

import com.ego.item.center.service.entity.TbItemDesc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品描述表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemDescMapper extends BaseMapper<TbItemDesc> {

}
