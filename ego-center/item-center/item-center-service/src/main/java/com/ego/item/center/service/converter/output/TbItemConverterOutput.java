package com.ego.item.center.service.converter.output;

import com.ego.item.center.api.dto.output.QueryTbItemDetailRpcOutput;
import com.ego.item.center.api.dto.output.TbItemPageListRpcOutput;
import com.ego.item.center.service.entity.TbItem;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

import java.util.List;

/**
 * @author lyy
 */
@Mapper(componentModel = "spring",nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface TbItemConverterOutput {

    /**
     * @param tbItem
     * @return
     */
    TbItemPageListRpcOutput entity2RpcOutput(TbItem tbItem);

    /**
     * @param tbItems
     * @return
     */
    List<TbItemPageListRpcOutput> toListRpcOutput(List<TbItem> tbItems);

    /**
     * @param tbItem
     * @return
     */
    QueryTbItemDetailRpcOutput entity2RpcDetailOuput(TbItem tbItem);

}
