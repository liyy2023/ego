package com.ego.item.center.service.service.impl;

import com.ego.item.center.service.entity.TbItemParam;
import com.ego.item.center.service.mapper.TbItemParamMapper;
import com.ego.item.center.service.service.TbItemParamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品规则参数 服务实现类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
@Service
public class TbItemParamServiceImpl extends ServiceImpl<TbItemParamMapper, TbItemParam> implements TbItemParamService {

}
