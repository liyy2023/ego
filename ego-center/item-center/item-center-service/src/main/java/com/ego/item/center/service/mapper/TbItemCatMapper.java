package com.ego.item.center.service.mapper;

import com.ego.item.center.service.entity.TbItemCat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品类目 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemCatMapper extends BaseMapper<TbItemCat> {

}
