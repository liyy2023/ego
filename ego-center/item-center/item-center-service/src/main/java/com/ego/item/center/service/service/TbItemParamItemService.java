package com.ego.item.center.service.service;

import com.ego.item.center.service.entity.TbItemParamItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品规格和商品的关系表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemParamItemService extends IService<TbItemParamItem> {

}
