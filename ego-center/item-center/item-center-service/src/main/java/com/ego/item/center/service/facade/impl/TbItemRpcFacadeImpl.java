package com.ego.item.center.service.facade.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ego.common.response.PageResponse;
import com.ego.common.response.RpcResponse;
import com.ego.item.center.api.dto.input.*;
import com.ego.item.center.api.dto.output.QueryTbItemDetailRpcOutput;
import com.ego.item.center.api.dto.output.TbItemPageListRpcOutput;
import com.ego.item.center.api.facade.TbItemRpcFacade;
import com.ego.item.center.service.converter.input.TbItemConverterInput;
import com.ego.item.center.service.converter.output.TbItemConverterOutput;
import com.ego.item.center.service.entity.TbItem;
import com.ego.item.center.service.entity.TbItemDesc;
import com.ego.item.center.service.service.TbItemDescService;
import com.ego.item.center.service.service.TbItemService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author lyy
 */
@Slf4j
@DubboService
public class TbItemRpcFacadeImpl implements TbItemRpcFacade {
    @Autowired
    TbItemService tbItemService;
    @Autowired
    TbItemDescService tbItemDescService;
    @Autowired
    TbItemConverterOutput tbItemConverterOutput;
    @Autowired
    TbItemConverterInput tbItemConverterInput;
    @Override
    public RpcResponse<PageResponse<TbItemPageListRpcOutput>> getTbItemPageList(TbItemPageListRpcInput rpcInput) {
        PageResponse<TbItemPageListRpcOutput> outputList = PageResponse.empty();
        //调用service
        IPage<TbItem> ipage = tbItemService.queryTbItemPageList(rpcInput);
        //出参转换
        outputList.setTotal(ipage.getTotal());
        outputList.setData(tbItemConverterOutput.toListRpcOutput(ipage.getRecords()));
        return RpcResponse.ok(outputList);
    }

    @Transactional
    @Override
    public RpcResponse<Boolean> insertOrUpdateTbItem(InsertTbItemRpcInput rpcInput) {
        TbItem tbItem = tbItemConverterInput.rpcInput2TbItem(rpcInput);
        if(null == rpcInput.getId()){
             //保存商品信息
            tbItem.setCreated(LocalDateTime.now());
            Boolean itemIsSave = tbItemService.save(tbItem);
            //更新商品描述
            Long itemId = tbItemService.queryItemIdByTitle(tbItem.getTitle());
            TbItemDesc tbItemDesc = TbItemDesc.builder().itemDesc(rpcInput.getDescription())
                    .created(LocalDateTime.now()).itemId(itemId).build();
            Boolean itemDescIsSave = tbItemDescService.save(tbItemDesc);
            if(itemIsSave && itemDescIsSave){
                return RpcResponse.ok(Boolean.TRUE);
            }else {
                return RpcResponse.ok(Boolean.FALSE);
            }
        }else {
            tbItem.setUpdated(LocalDateTime.now());
            Boolean itemIsUpdate = tbItemService.updateById(tbItem);
            TbItemDesc tbItemDesc = TbItemDesc.builder().itemDesc(rpcInput.getDescription())
                    .updated(LocalDateTime.now()).itemId(tbItem.getId()).build();
            Boolean itemDescIsUpdate = tbItemDescService.saveOrUpdate(tbItemDesc);
            if(itemIsUpdate && itemDescIsUpdate){
                return RpcResponse.ok(Boolean.TRUE);
            }else {
                return RpcResponse.ok(Boolean.FALSE);
            }
        }
    }

    @Override
    public RpcResponse<Boolean> updateTbItemStatus(UpdateTbItemStatusRpcInput rpcInput) {
        Boolean result = tbItemService.updateTbItemStatus(rpcInput);
        return RpcResponse.ok(result);
    }

    @Override
    public RpcResponse<QueryTbItemDetailRpcOutput> queryTbItemDetail(ByIdRpcInput rpcInput) {
        TbItem tbItem = tbItemService.getById(rpcInput.getId());
        TbItemDesc tbItemDesc = tbItemDescService.getById(rpcInput.getId());
        QueryTbItemDetailRpcOutput rpcOutput = tbItemConverterOutput.entity2RpcDetailOuput(tbItem);
        rpcOutput.setDescription(tbItemDesc.getItemDesc());
        return RpcResponse.ok(rpcOutput);
    }
}
