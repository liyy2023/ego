package com.ego.item.center.service.mapper;

import com.ego.item.center.service.entity.TbItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemMapper extends BaseMapper<TbItem> {
    /**
     * @param title
     * @return
     */
    Long queryItemIdByTitle(@Param("title") String title);
}
