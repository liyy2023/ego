package com.ego.item.center.service.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ego.item.center.api.dto.input.TbItemPageListRpcInput;
import com.ego.item.center.api.dto.input.UpdateTbItemStatusRpcInput;
import com.ego.item.center.service.entity.TbItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemService extends IService<TbItem> {

    /**
     * 分页查询商品
     * @return
     * @param rpcInput
     */
    IPage<TbItem> queryTbItemPageList(TbItemPageListRpcInput rpcInput);
    /**
     * 更新商品状态
     * @return
     * @param rpcInput
     */
    Boolean updateTbItemStatus(UpdateTbItemStatusRpcInput rpcInput);

    Long queryItemIdByTitle(String title);



}
