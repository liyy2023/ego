package com.ego.item.center.service.mapper;

import com.ego.item.center.service.entity.TbItemParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品规则参数 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemParamMapper extends BaseMapper<TbItemParam> {

}
