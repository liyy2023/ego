package com.ego.item.center.service.mapper;

import com.ego.item.center.service.entity.TbItemParamItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品规格和商品的关系表 Mapper 接口
 * </p>
 *
 * @author Mybatis-plus3.x
 * @since 2021-04-29
 */
public interface TbItemParamItemMapper extends BaseMapper<TbItemParamItem> {

}
