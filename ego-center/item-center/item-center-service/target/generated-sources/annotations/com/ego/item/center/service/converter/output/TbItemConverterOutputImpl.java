package com.ego.item.center.service.converter.output;

import com.ego.item.center.api.dto.output.QueryTbItemDetailRpcOutput;
import com.ego.item.center.api.dto.output.TbItemPageListRpcOutput;
import com.ego.item.center.service.entity.TbItem;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-04-30T16:22:36+0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_181 (Oracle Corporation)"
)
@Component
public class TbItemConverterOutputImpl implements TbItemConverterOutput {

    @Override
    public TbItemPageListRpcOutput entity2RpcOutput(TbItem tbItem) {

        TbItemPageListRpcOutput tbItemPageListRpcOutput = new TbItemPageListRpcOutput();

        if ( tbItem != null ) {
            tbItemPageListRpcOutput.setTitle( tbItem.getTitle() );
            tbItemPageListRpcOutput.setSellPoint( tbItem.getSellPoint() );
            tbItemPageListRpcOutput.setPrice( tbItem.getPrice() );
            tbItemPageListRpcOutput.setNum( tbItem.getNum() );
            tbItemPageListRpcOutput.setBarcode( tbItem.getBarcode() );
            tbItemPageListRpcOutput.setImage( tbItem.getImage() );
            tbItemPageListRpcOutput.setCid( tbItem.getCid() );
        }

        return tbItemPageListRpcOutput;
    }

    @Override
    public List<TbItemPageListRpcOutput> toListRpcOutput(List<TbItem> tbItems) {
        if ( tbItems == null ) {
            return new ArrayList<TbItemPageListRpcOutput>();
        }

        List<TbItemPageListRpcOutput> list = new ArrayList<TbItemPageListRpcOutput>( tbItems.size() );
        for ( TbItem tbItem : tbItems ) {
            list.add( entity2RpcOutput( tbItem ) );
        }

        return list;
    }

    @Override
    public QueryTbItemDetailRpcOutput entity2RpcDetailOuput(TbItem tbItem) {

        QueryTbItemDetailRpcOutput queryTbItemDetailRpcOutput = new QueryTbItemDetailRpcOutput();

        if ( tbItem != null ) {
            queryTbItemDetailRpcOutput.setId( tbItem.getId() );
            queryTbItemDetailRpcOutput.setTitle( tbItem.getTitle() );
            queryTbItemDetailRpcOutput.setSellPoint( tbItem.getSellPoint() );
            queryTbItemDetailRpcOutput.setPrice( tbItem.getPrice() );
            queryTbItemDetailRpcOutput.setNum( tbItem.getNum() );
            queryTbItemDetailRpcOutput.setBarcode( tbItem.getBarcode() );
            queryTbItemDetailRpcOutput.setImage( tbItem.getImage() );
            queryTbItemDetailRpcOutput.setCid( tbItem.getCid() );
        }

        return queryTbItemDetailRpcOutput;
    }
}
