package com.ego.item.center.service.converter.input;

import com.ego.item.center.api.dto.input.InsertTbItemRpcInput;
import com.ego.item.center.service.entity.TbItem;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-04-30T16:22:36+0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_181 (Oracle Corporation)"
)
@Component
public class TbItemConverterInputImpl implements TbItemConverterInput {

    @Override
    public TbItem rpcInput2TbItem(InsertTbItemRpcInput rpcInput) {

        TbItem tbItem = new TbItem();

        if ( rpcInput != null ) {
            tbItem.setId( rpcInput.getId() );
            tbItem.setTitle( rpcInput.getTitle() );
            tbItem.setSellPoint( rpcInput.getSellPoint() );
            tbItem.setPrice( rpcInput.getPrice() );
            tbItem.setNum( rpcInput.getNum() );
            tbItem.setBarcode( rpcInput.getBarcode() );
            tbItem.setImage( rpcInput.getImage() );
            tbItem.setCid( rpcInput.getCid() );
        }

        return tbItem;
    }
}
