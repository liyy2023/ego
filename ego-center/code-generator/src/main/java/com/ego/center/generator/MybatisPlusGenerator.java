package com.ego.center.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 14:30:19
 **/
public class MybatisPlusGenerator {

    private static final MybatisPlusGenerator instance = new MybatisPlusGenerator();
    private static String DATASOURCE_URL;
    private static String DATASOURCE_USERNAME;
    private static String DATASOURCE_PASSWORD;
    private static String DATASOURCE_DRIVER_NAME;
    private static String PARENT_PACKAGE_NAME;
    private static String MODULE_PATH = "";

    private static Boolean GENERATOR_ENTITY;
    private static Boolean GENERATOR_MAPPER;
    private static Boolean GENERATOR_MAPPER_XML;
    private static Boolean GENERATOR_SERVICE;
    private static Boolean GENERATOR_SERVICE_IMPL;

    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (!StringUtils.isEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) throws IOException {
        //初始化数据源
        init();
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir") + MODULE_PATH;
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("Mybatis-plus3.x");
        //是否打开输出目录
        gc.setOpen(false);
        gc.setIdType(IdType.AUTO);
        // 开启 ActiveRecord 模式
        gc.setActiveRecord(false);
        gc.setBaseColumnList(true);
        //设置文件是否覆盖
        gc.setFileOverride(true);
        //实体属性 Swagger2 注解
         gc.setSwagger2(true);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setEntityName("%s");
        gc.setServiceName("%sService");
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(MybatisPlusGenerator.DATASOURCE_URL);
        dsc.setDriverName(DATASOURCE_DRIVER_NAME);
        dsc.setUsername(DATASOURCE_USERNAME);
        dsc.setPassword(DATASOURCE_PASSWORD);
        mpg.setDataSource(dsc);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENT_PACKAGE_NAME);
        //设置模块名-->任何一个模块如果设置 空 OR Null 将不生成该模块。
        // pc.setModuleName("employee");
//        pc.setEntity("entity");
//        pc.setController("controller");
//        pc.setService("service");
//        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);
        if(GENERATOR_MAPPER_XML){
            // 自定义配置
            InjectionConfig cfg = new InjectionConfig() {
                @Override
                public void initMap() {
                    // to do nothing
                }
            };
            // 如果模板引擎是 freemarker
            String templatePath = "/templates/mapper.xml.vm";
            // 自定义输出配置
            List<FileOutConfig> focList = new ArrayList<>();
            // 自定义配置会被优先输出
            focList.add(new FileOutConfig(templatePath) {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                    return projectPath + "/src/main/resources/mapper/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
                }
            });
            cfg.setFileOutConfigList(focList);
            mpg.setCfg(cfg);
        }
        // 配置模板 可以自定义生成那些模块
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        // 不生成conrc_teacher_certificatetroller层
        templateConfig.setController(null);
        if(!GENERATOR_ENTITY){
            templateConfig.setEntity(null);
        }
        if(!GENERATOR_MAPPER){
            templateConfig.setMapper(null);
        }
        if(!GENERATOR_SERVICE){
            templateConfig.setService(null);
        }
        if(!GENERATOR_SERVICE_IMPL){
            templateConfig.setServiceImpl(null);
        }
        mpg.setTemplate(templateConfig);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 是否使用Lombok优化代码
        strategy.setEntityLombokModel(false);
        // controller类是否直接返回json
        strategy.setRestControllerStyle(true);
        // 写于父类中的公共字段
        // strategy.setSuperEntityColumns("id");
        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        //表名，多个英文逗号分割
        strategy.setControllerMappingHyphenStyle(true);
        //strategy.setTablePrefix(pc.getModuleName() + "_");

        //生成的类名去掉前缀 如yw_sys_user ---> SysUser
//        strategy.setTablePrefix("t_");
        mpg.setStrategy(strategy);
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

    public static void init() {
        Yaml yaml = new Yaml();
        InputStream in = MybatisPlusGenerator.class.getClassLoader().getResourceAsStream("application.yml");
        Map<String, Object> properties = yaml.load(in);
        DATASOURCE_URL = (String) getValueByKey("spring.datasource.url", properties);
        DATASOURCE_USERNAME = (String) getValueByKey("spring.datasource.username", properties);
        DATASOURCE_PASSWORD = getValueByKey("spring.datasource.password", properties).toString();
        DATASOURCE_DRIVER_NAME = getValueByKey("spring.datasource.driver-class-name", properties).toString();
        PARENT_PACKAGE_NAME = (String) getValueByKey("mybatisPlus.parentPackageName", properties);
        MODULE_PATH = (String)getValueByKey("mybatisPlus.modulePath",properties);
        GENERATOR_ENTITY = (Boolean) getValueByKey("mybatisPlus.generator.entity", properties);
        GENERATOR_MAPPER = (Boolean) getValueByKey("mybatisPlus.generator.mapper", properties);
        GENERATOR_MAPPER_XML = (Boolean) getValueByKey("mybatisPlus.generator.mapperXml", properties);
        GENERATOR_SERVICE = (Boolean) getValueByKey("mybatisPlus.generator.service", properties);
        GENERATOR_SERVICE_IMPL = (Boolean) getValueByKey("mybatisPlus.generator.serviceImpl", properties);
    }

    public static Object getValueByKey(String key, Map<String, Object> properties) {
        String separator = ".";
        String[] separatorKeys = null;
        if (key.contains(separator)) {
            separatorKeys = key.split("\\.");
        } else {
            return properties.get(key);
        }
        Map<String, Map<String, Object>> finalValue = new HashMap<>();
        for (int i = 0; i < separatorKeys.length - 1; i++) {
            if (i == 0) {
                finalValue = (Map) properties.get(separatorKeys[i]);
                continue;
            }
            if (finalValue == null) {
                break;
            }
            finalValue = (Map) finalValue.get(separatorKeys[i]);
        }
        return finalValue == null ? null : finalValue.get(separatorKeys[separatorKeys.length - 1]);
    }
}
