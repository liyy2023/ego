package com.ego.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:15:30
 **/
@SpringBootApplication(scanBasePackages = {
        "com.ego",
        "com.ego.*.center.api",
        "com.ego.*.center.service",
        "com.ego.center.config"
})
public class CenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(CenterApplication.class,args);
    }
}
