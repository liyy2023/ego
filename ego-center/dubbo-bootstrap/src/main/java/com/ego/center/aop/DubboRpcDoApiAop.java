package com.ego.center.aop;

import com.ego.common.aop.AbstractOpenApiAop;
import com.ego.common.base.ServerInterfaceLog;
import com.ego.common.exception.EgoException;
import com.ego.common.request.AbstractRequest;
import com.ego.common.response.RpcResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:16:44
 **/
@Aspect
@Component
@Order(0)
public class DubboRpcDoApiAop extends AbstractOpenApiAop {
    @Override
    @Pointcut("execution(* com.ego.*.center.api..*.*(..))")
    public void apiPointcut() {
    }

    @Override
    @Around("apiPointcut()")
    public Object doApi(ProceedingJoinPoint joinPoint) {
        return super.doApi(joinPoint);
    }


//    @Override
    protected Object failWithCodeCheck(AbstractRequest request, EgoException egoException) {
        return RpcResponse.fail(egoException.getResponseCodeEnum().getCode(),
                EgoException.detailMessage(egoException.getResponseCodeEnum(),egoException.getArgs()));
    }


    @Override
    protected void updateInterfaceLog(ServerInterfaceLog serverInterfaceLog, AbstractRequest abstractRequest) {

    }
}
