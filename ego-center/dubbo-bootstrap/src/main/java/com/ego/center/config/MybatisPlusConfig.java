package com.ego.center.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @author lyy
 * @description
 * @date 2021-4-29 16:14:02
 **/
@Configuration
@MapperScan("com.ego.*.center.service.mapper")
@EnableTransactionManagement
@Slf4j
public class MybatisPlusConfig {

    @Autowired
    private DataSource dataSource;
    /**
     * 分页插件
     */
    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        log.info("加载分页插件");
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager(){
        log.info("初始化transactionManager");
        log.info(String.valueOf(dataSource.getClass()));
        return new DataSourceTransactionManager(dataSource);
    }
}
