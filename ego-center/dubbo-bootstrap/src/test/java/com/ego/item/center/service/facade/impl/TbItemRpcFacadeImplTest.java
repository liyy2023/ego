package com.ego.item.center.service.facade.impl;

import com.ego.center.CenterApplication;
import com.ego.common.base.PageInput;
import com.ego.common.response.PageResponse;
import com.ego.item.center.api.dto.input.TbItemPageListRpcInput;
import com.ego.item.center.api.dto.output.TbItemPageListRpcOutput;
import com.ego.item.center.api.facade.TbItemRpcFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CenterApplication.class)
public class TbItemRpcFacadeImplTest {
    @Autowired
    TbItemRpcFacade tbItemRpcFacade;

    @Test
    public void test(){
        PageInput pageInput = new PageInput();
        pageInput.setPageNo(1);
        pageInput.setPageSize(10);
        TbItemPageListRpcInput rpcInput = new TbItemPageListRpcInput();
        rpcInput.setPageType(pageInput);
        PageResponse<TbItemPageListRpcOutput> response = tbItemRpcFacade.getTbItemPageList(rpcInput).getData();
        System.out.println("response.getData() = " + response.getData());


    }

}